

const express = require('express');
//import express module in the application


const app = express()	//createServer()
// express() is the server stored in the variable app

const PORT = 3005;

// Middlewares
app.use(express.json())
app.use(express.urlencoded({extended:true}))


// HANDLING ROUTES WITH HTTP METHOD
// app.method(<endpoint>, <request lister function>)
app.get("/", (req, res) => res.status(202).send("Hello from get route") )


app.post("/hello", (req, res) => { 

	console.log(req.body)

	// console.log(typeof req.body)
	const {firstName, lastName} = req.body


	res.send(`Hello, my name is ${firstName} ${lastName}!`)
})

const user = [
	{
		"userName": "joypague",
		"password": "123"
	},
	{
		"userName": "anghelito",
		"password": "456"
	}
];
// (create mock database)

app.post("/signup", (req, res) => {
	// create a route for "/signup" endpoint that will add a document to a mock database. 
	const {userName, password} = req.body

	if(userName !== "" && password !== ""){
		
		user.push(req.body)
		
		console.log(user)

		res.send(`User ${userName} successfully registered.`)
		

	} else {
		res.status(400).send(`Please input both Username and Password.`)
	}
})

app.patch('/change-password', (req, res) => {
	// console.log(req)
	const {userName, password} = req.body

	let message;

	for(let i = 0; i < user.length; i++){

		if(user[i].userName === userName){
			user[i].password = password

			message = `User ${userName}'s password has been updated`

			console.log(user)

			break;

		} else {

			message = `User does not exist`
		}
	}


	res.status(200).send(message)
})


// LISTEN METHOD
app.listen(PORT, () => console.log(`Server connected to port ${PORT}`))
